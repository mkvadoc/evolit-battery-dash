import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GetDataService {
  private apiUrl = "http://demo.qant.eu/bmsInfo";
  modules: any;
  constructor(private http: HttpClient) { }

  getData() {
    return this.http.get(this.apiUrl);
  }

}
