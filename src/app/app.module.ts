import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ModulesComponent } from './components/modules/modules.component';
import { WidgetComponent } from './components/widgets/widget/widget.component';
import { WidgetsComponent } from './components/widgets/widgets.component';
import { SumPipe } from './pipes/sum.pipe';
import { FindMaxPipe } from './pipes/find-max.pipe';
import { FindMinPipePipe } from './pipes/find-min.pipe';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SumPipe,
    ModulesComponent,
    WidgetComponent,
    WidgetsComponent,
    FindMaxPipe,
    FindMinPipePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
