import { outputAst } from '@angular/compiler';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'findMax',
  pure: true,
})
export class FindMaxPipe implements PipeTransform {

  transform(moduleData: any,) {
    return moduleData.modules.reduce((totalMax: any, currentModul: any) => {
      const maxModulVolatage = currentModul.cells.reduce((prev: any, { voltage }: any) => (prev > voltage) ? prev : voltage, 0);
      return totalMax > maxModulVolatage ? totalMax : maxModulVolatage
    })
  }
}

