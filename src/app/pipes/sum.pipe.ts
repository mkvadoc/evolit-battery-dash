import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'voltageSum'
})

export class SumPipe implements PipeTransform {
  transform(cells: any[]): any {
    return cells.reduce((resultVoltage, {voltage}) => resultVoltage + voltage, 0);
  }
}