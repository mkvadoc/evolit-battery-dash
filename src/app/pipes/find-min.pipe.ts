import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'findMin',
  pure: true,
})
export class FindMinPipePipe implements PipeTransform {

  transform(moduleData: any,) {
    return moduleData.modules.reduce((totalMin: any, currentModul: any) => {
      const minModulVolatage = currentModul.cells.reduce((prev: any, { voltage }: any) => (prev < voltage) ? prev : voltage);
      return totalMin > minModulVolatage ? totalMin : minModulVolatage
    })
  }
}
