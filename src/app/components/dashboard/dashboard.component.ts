import { Component, OnInit } from '@angular/core';
import { GetDataService } from 'src/app/services/get-data.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public moduleData: any = {};

  constructor(private dataService: GetDataService) {
   }

  ngOnInit(): void {
   this.moduleData = this.dataService.getData().subscribe(data => {
        this.moduleData = data;})
  }

}
