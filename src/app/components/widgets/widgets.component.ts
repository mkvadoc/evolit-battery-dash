import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-widgets',
  templateUrl: './widgets.component.html',
  styleUrls: ['./widgets.component.css']
})
export class WidgetsComponent implements OnInit {
  @Input() moduleData:any= {};
  
  title: string = this.moduleData;
  value: any;
  constructor() { }

  ngOnInit(): void {
  }

}
